Some basic git commands are used when we install and update the ITSDAQ. Here is the 
[Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf).

Procedures of installing and updating the ITSDAQ:

- Download the ITSDAQ project
 
  `git clone https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git /opt/itsdaq/itsdaq-sw`

  Input your username and password of gitlab.cern.ch and download the ITSDAQ to `/opt/itsdaq/itsdaq-sw`.

- Display the version of the ITSDAQ
  
  `git log`

  The top one diplayed on the screen is the version of ITSDAQ we currently use. And enter `q` to quit.

  Or use the command `git log -n 1` to only display the top one.

- Install the ITSDAQ

  `python waf configure` (or `python waf configure --enable-visa` for the single module test setup)

  `python waf build`

  `python waf install`

- Upgarde the ITSDAQ
  
  `git pull` (If there is no conflict to resolve)

  Input your username and password of gitlab.cern.ch to upgarde the ITSDAQ project.

  `python waf install`
