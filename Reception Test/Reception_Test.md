# Reception Test

1. Load the modules (wire bonds pulled to have different bonded_IDs) onto the coldjig

2. Connect the cables

3. Edit the configure files in coldbox PC ( heplnw050 )

    - `cd /data/sctvar/config`
    - Generate the hybrid configure files from database
    - Edit the st_system_config.dat
        - Comment the module information of old tests
        - Add the module information of new tests
            - LS module
            - SS module

4. Let the LV power supply output

5. Turn on ITSDAQ in coldbox PC ( heplnw050 ), configure and test the AMACs
    - `cd /opt/itsdaq/itsdaq-sw`
    - `source ../setup_itsdaq_root6.sh`
    - `./RUNITSDAQ.sh`
    - Enter your name and enter “1” to confirm
    - `loadmacro("AMACStar")`
    -  AMACStar_initialiseAMACs({bonded_IDs},{channel_IDs})
        - e.g. `AMACStar_initialiseAMACs({0,1,2,3,4},{0,1,2,3,4})`
        - e.g. `AMACStar_initialiseAMACs({5,2,0},{1,3,4})`
    - `AMACStar_configureAMACs(0)`
    - `AMACStar_readReg(-1,40)`
        - The results should return non-zero headers and bonded IDs if working

6. Close the Coldbox

7. Turn on the N2 (> 8sL/min) and the chiller (20℃ pump: 5)

8. Turn on the WebGUI and Grafana
    - Raspberry Pi ( pi@heprp231 passwd: warwickCV47AL )
        - `cd ~/work/coldbox_controller_webgui`
        - `pipenv shell`
        - `./run_warwick.sh`
    - Browser
        - WebGUI: http://130.246.41.231:5000
            - Click on the “Start” button and wait until the heart beats
        - Grafana dashboard: https://ppdlabmonitor.pp.rl.ac.uk/d/XYlA4dlnz/uk_china_coldjig_influx2_dashboard?orgID=1&refresh=5s

9. Wait until the temperature and RH meet the requirement (21±2℃, <5%) 

10. Run IV Scan of modules one by one
    - Switch off the HV of all the modules
        - `AMACStar_writeReg(-1,40,0x00007700); AMACStar_readReg(-1,40);`
    - Switch on the HV of one module: AMACStar_writeReg(i,40,0x07077700)
        - i: order number of AMAC (i=-1: all)
            - e.g. AMACStar_initialiseAMACs({0,1,2,3,4},{0,1,2,3,4}): i = 0, 1, 2, 3, 4
            - e.g. AMACStar_initialiseAMACs({5,2,0},{1,3,4}): i = 0, 1, 2
        - e.g. `AMACStar_writeReg(0,40,0x07077700); AMACStar_readReg(-1,40);`
    - Click on the “DCS” option and then click on the “Module IV curve (through powerboard, 550V)” option
        - Change the “vEnd” to 0V
        - Change the “iLimit (add)” to 100μA
        - Enter the “Wafer” number and the “Module SN” according to the database
        - Select the “Module Stage” to “AT_LOADING_SITE”
        - Select the module “Type”
        - Enter the “Temperature” of VC and “Humidity” read from the Grafana dashboard
        - Click on the “OK” button
    - Check if the IV curve is good or not
    - Repeat the steps above until all the modules have been run the IV scan

11.	`AMACStar_configureAMACs(0)`

12.	`HVSupplies[0]->IVScanWithPB(0,-350,-10,-350,100,1000,0,5); //5: 5 PBs`

13.	`AMACStar_configureAMACs(1); //turn on DCDC`

14.	Click on the “Capture” option and then click on the “Scan HPR Output phase (LCB/R3L1)”

15.	Check if the result of HPR test is good or not

16.	Click on the “Test” option and then click on the “Full Test”

17.	Check if the results of full test are good or not.

18.	`HVSupplies[0]->Ramp(0,200)`

19.	Quit the ITSDAQ
    - `.q`

20.	Turn off the LV power supply

21.	Click on the “Shutdown” button on the WebGUI

22.	Turn off the chiller and the N2

23.	Open the coldbox
