Procedures:

[Install and upgrade ITSDAQ](https://gitlab.cern.ch/yebo/itk_strip_module_procedure_document/-/blob/master/ITSDAQ/itsdaq.md)

[Single panel test](https://gitlab.cern.ch/yebo/itk_strip_module_procedure_document/-/blob/master/Single%20panel%20test/single_panel_test.md)

[Single module test](https://gitlab.cern.ch/yebo/itk_strip_module_procedure_document/-/blob/master/Single%20module%20test/single_module_test.md)

