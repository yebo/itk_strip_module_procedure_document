Picture: PPA module

- Set up

  1. Load the module on the cooling jig

  2. Shield the module

  3. Screw the module on the cooling jig

  4. Get the cooling jig grounded through the screw

  5. Connect the cables

  6. Remove the shield

     <img src="module.jpg" width=50%>

     - LV power supply
       - Voltage: 11.000V
       - Current compliance: 1.000A

- Log in the PPD Linux PC heplnw019
  - Version of ITSDAQ software: commit ebb424bd51a39ecb8ecd9354e4644586b85aed86

  - Version of firmware: nexysv_itsdaq_vb4f7_FDP_STAR.bit
    - Refresh the firmware
      
      `djtgcfg prog -d NexysVideo -f /opt/itsdaq/firmware/nexysv_itsdaq_vb4f7_FDP_STAR.bit -i 0`

  - Paths of ITSDAQ
    
    ${SCTDAQ_ROOT}=/opt/itsdaq/itsdaq-sw/, ${SCTDAQ_VAR}=/data/sctvar/
  
  - Set the permissions of devices
    
    `sudo chmod a+rw /dev/tty*`

- Configure files

  TODO

- Output the LV
  - Press the output button or use the commands below to control the LV power supply
    
    `LVSupplies[1]->On()`

    `LVSupplies[1]->Off()`

    `LVSupplies[1]->PrintStatus()`

     - 1: Order number (second here) of power supply listed in /data/sctvar/config/power_supplies.json

- ITSDAQ
  
  1. Start ITSDAQ
     
     `cd /opt/itsdaq/itsdaq-sw`

     `source ../setup_itsdaq_root6.sh`

     `./RUNITSDAQ.sh`

      - Input username at the command line 
      - Input '1' at the command line to confirm

  2. Load commands for AMAC control
     
     `loadmacro("AMACStar")`

  3. Initialize the AMACs

     `AMACStar_initialiseAMACs({0},{0})`

      - 0: bonded ID of powebord
      - 0: on channel 0

  4. Configure the AMACs leaving the DCDCs off

     `AMACStar_configureAMACs(0)`

      - 0: DCDCs off
  
  5. Check communication with the AMAC by reading register
      
     `AMACStar_readReg(-1,41)`

      - -1: send command to all AMACs
      - 41: read register 41
      - The result should return the non-zero headers and bonded IDs if working

  6. Light sensitivity test
      - Unplug the cable from the HV power supply and plug it to the multimeter
      - Close the box and put the blanket on it
      - The voltage drops down if the connection is good
        - e.g. 0.4871 VDC -> 0.0599 VDC
      - Unplug the cable from the multimeter and plug it back to the HV power supply

  7. Tighten the screws on the box

  8. Open the rotor meter to supply the nitrogen

  9. Slightly loosen the screw at one corner until you hear the sound of nitrogen leakage
  
  10. Put the blanket on the box

  11. Open the chiller

  12. Use DCSData software (or Grafana) to monitor the temperature and the humidity
      
      - Wait until the temperature is 21±1 ℃ and the humidity is less than 5%
  
  13. IV scan of BONDED module (HV_TAB_ATTACHED module reception IV scan is different)
      
      - Make sure you have the ${SCTDAQ_VAR}/config/IVScanConfig.txt 
      
      - Click on the “DCS” option on the menu of GUI and select “Module IV curve (through powerboard, 550V)” option

      - Set the IV scan configuration
        - IV scan will ramp the voltage up from 0 to -550V, and then ramp the voltage down to -350V. The full test should be run at -350V.
          - Ramping step: -10V
          - Ramping factor: 3 (default)
          - The current recorded delay: 10000ms
          - Current compliance: 
             11μA [iLimit (add)] + [bias voltage] / 10MΩ [resistor in PB]
        - Wafer number can be found on the database
        - Module serial number on the database
        - Select “GLUED” when do the single module test
        - Run number will be recorded automatically, leave it blank
        - Select the correct module type
        - Temperature and humidity monitored

      - The plot of IV curve will be shown on the screen if it finishes
      
      - The bias voltage will be -350V when IV scan finishes

  14. Turn the DCDC on after the IV scan (It will generate some heat)
     
     `AMACStar_configureAMACs(1)` or `AMACStar_DCDC(-1,1)`
      - 1: DCDC on

  15. Click on the “Capture” option and then click on the “Scan HPR Output phase (LCB/R3L1)"
  16. Check if the result of HPR test is good or not
  17. Click on the “Test” option and then click on the “Full Test”
  18. Check if the results of full test are good or not.
  19. Ramp down the HV
      
      `HVSupplies[0]->Ramp(0,46) // default ramping factor 3`
       - 0: Order number (first here) of power supply listed in $/data/sctvar/config/power_supplies.json
       - 0: Ramp to 0V
       - 46: Current compliance is 46 μA at -350V

  20. Quit the ITSDAQ
      
      `.q`

  21. Turn off the LV power supply

