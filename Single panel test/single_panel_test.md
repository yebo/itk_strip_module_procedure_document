Picture: BHM_P10

- Set up

  1. Load the panel on the jig
  2. Load the powerboards on the panel
     
     <img src="PB_pickup_tool.jpg" width=40%>
     <img src="panel_with_PBs.jpg" width=36%>

     - Do not tighten the screws so heavily that the powerboard will be tilted or bended

  3. Shielding and connection

     <img src="setup.jpg" width=80%>

     - LV power supply
       - Voltage: 11.000V
       - Current compliance: 2.500A

- Log in the PPD Linux PC heplnw023
  - Version of ITSDAQ software: commit ebb424bd51a39ecb8ecd9354e4644586b85aed86

  - Version of firmware: nexysv_itsdaq_vb5c8_FIB_STAR_EMUHV1.bit
    - Refresh the firmware
      
      `djtgcfg prog -d NexysVideo -f /opt/itsdaq/firmware/nexysv_itsdaq_vb5c8_FIB_STAR_EMUHV1.bit -i 0`

  - Paths of ITSDAQ
    
    ${SCTDAQ_ROOT}=/opt/itsdaq/itsdaq-sw/, ${SCTDAQ_VAR}=/data/sctvar/
  
  - Set the permissions of devices
    
    `sudo chmod a+rw /dev/tty*`

- Configure files

  TODO

- Output the LV
  - Press the output button or use the commands below to control the LV power supply
    
    `LVSupplies[0]->On()`

    `LVSupplies[0]->Off()`

    `LVSupplies[0]->PrintStatus()`

     - 0: Order number (first here) of power supply listed in /data/sctvar/config/power_supplies.json

- ITSDAQ
  
  1. Start ITSDAQ
     
     `cd /opt/itsdaq/itsdaq-sw`

     `source ../setup_itsdaq_root6.sh`

     `./RUNITSDAQ.sh`

      - Input username at the command line 
      - Input '1' at the command line to confirm

  2. Load commands for AMAC control
     
     `loadmacro("AMACv2_final")`

  3. Initialize the AMACs

     `AMACv2_final_initialiseAMACs({0,1,4},{0,0,0})`

      - 0,1,4: bonded IDs of powebords
      - 0,0,0: all on channel 0

  4. Configure the AMACs leaving the DCDCs off

     `AMACv2_final_configureAMACs(0)`

      - 0: DCDCs off
  
  5. Check communication with the AMAC by reading register
      
     `AMACv2_final_readReg(-1,46)`

      - -1: send command to all AMACs
      - 46: read register 46
      - The result should return the non-zero headers and bonded IDs if working

  6. Turn on DCDC
     
     `AMACv2_final_configureAMACs(1)` or `AMACv2_final_DCDC(-1,1)`
      - 1: DCDC on

  7. Click on the “Capture” option and then click on the “Scan HPR Output phase (LCB/R3L1)"
  8. Check if the result of HPR test is good or not
  9. Click on the “Test” option and then click on the “Full Test”
  10. Check if the results of full test are good or not.
  11. Quit the ITSDAQ
      
      `.q`

  12. Turn off the LV power supply



	


